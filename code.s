PORTB = $6000
PORTA = $6001
DDRB = $6002
DDRA = $6003

E  = %10000000
RW = %01000000
RS = %00100000

	macro writechar
	lda \1
	sta PORTB

	lda #RS
	sta PORTA

	lda #(E | RS)
	sta PORTA

	lda #RS
	sta PORTA
	endmacro

	org $8000
reset:
	ldx #$ff
	txs

	lda #%11111111
	sta DDRB

	lda #%11100000
	sta DDRA

	lda #%00000001
  jsr lcd_command

	lda #%00111000
  jsr lcd_command

	lda #%00001110
  jsr lcd_command

	lda #%00000110
  jsr lcd_command

	;; lda #"H"
	;; sta PORTB

	;; lda #RS
	;; sta PORTA

	;; lda #(E | RS)
	;; sta PORTA

	;; lda #RS
	;; sta PORTA
	lda #"H"
  jsr output_char
	lda #"e"
  jsr output_char
	lda #"l"
  jsr output_char
	lda #"l"
  jsr output_char
	lda #"o"
  jsr output_char
	lda #","
  jsr output_char
	lda #" "
  jsr output_char
	lda #"w"
  jsr output_char
	lda #"o"
  jsr output_char
	lda #"r"
  jsr output_char
	lda #"l"
  jsr output_char
	lda #"d"
  jsr output_char
	lda #"!"
  jsr output_char
loop:
	jmp loop

lcd_command:
  sta PORTB

	lda #0
	sta PORTA

	lda #E
	sta PORTA

	lda #0
	sta PORTA

  rts

output_char:
  sta PORTB

	lda #RS
	sta PORTA

	lda #(E | RS)
	sta PORTA

	lda #RS
	sta PORTA

  rts

	org $fffc
	word reset
	word $0000

